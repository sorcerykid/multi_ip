Multiplayer Limit Mod
By Leslie E. Krause

Prevent multiple players from joining a server simultaneously from a single IP address.
Players with with "multi_ip" privilege can bypass this restriction.

NB: Sometimes two or more players may share the same IP address if they are connecting
via a LAN, such as in a school or family setting. It is recommended to use discretion
since this may block legitimate players under such circumstances.


Repository
----------------------

Browse source code...
  https://bitbucket.org/sorcerykid/multi_ip

Download archive...
  https://bitbucket.org/sorcerykid/multi_ip/get/master.zip
  https://bitbucket.org/sorcerykid/multi_ip/get/master.tar.gz

Installation
----------------------

  1) Unzip the archive into the mods directory of your game
  2) Rename the multi_ip-master directory to "multi_ip"

Source Code License
----------------------

The MIT License (MIT)

Copyright (c) 2021, Leslie Krause (leslie@searstower.org)

Permission is hereby granted, free of charge, to any person obtaining a copy of this
software and associated documentation files (the "Software"), to deal in the Software
without restriction, including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

For more details:
https://opensource.org/licenses/MIT

