--------------------------------------------------------
-- Minetest :: Multi IP (multi_ip)
--
-- See README.txt for licensing and release notes.
-- Copyright (c) 2021 Leslie E. Krause
--
-- ./games/just_test_tribute/mods/multi_ip/init.lua
--------------------------------------------------------

local addrs = { }

minetest.register_privilege( "multi_ip", {
        description = "Permit multiple logins from the same IP address.",
} )

minetest.register_on_joinplayer( function ( player )
	local name = player:get_player_name( )
	local ip = minetest.get_player_information( name ).address

	if addrs[ ip ] and not minetest.check_player_privs( name, { bypass_addr = true } ) then
		minetest.kick_player( name, "Multiple sessions from the same IP address are not permitted." )
	else
		addrs[ ip ] = name
	end
end )

minetest.register_on_leaveplayer( function ( player )
	local name = player:get_player_name( )

	for k, v in pairs( addrs ) do
		if v == name then
			addrs[ k ] = nil
		end
	end
end )
